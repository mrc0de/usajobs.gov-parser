## Job Parser


```
Job Parser is a CLI(Command-Line-Interface) driven application consisting functions
that allow a user to easily parse certain information from a website. This specific 
script focuses on parsing information from USAJobs.gov.

02/14/16 - Currently, allows you to get the duplicate keywords on a USAJobs.gov for qualifications. 
	-It currently parses the words that occur: 
		-One time
		-Two times
		-Three times
		-Four Times
		-Five Times
		-Six Times
		-Seven Times.

02/15/16 - Support for duties sections of USAJobs now. It exports to (2) seperate text files.
One for the qualifications, and one for duties.
```

#### Job Parser was built on the premises of :####
- USAJobs makes identifying good jobs to apply to, difficult. 
- Its tough to know if you really **are** a good fit for a position, without a lot of 
reading through *often verbose* job descriptions.

#### Installation/Use####
**Installation:**
- Install virtualenv (`pip install virtualenv`)
- Install Python 3+ (depends on your `os`)

**Use:**
- **Run** Python intepreter: `python` (within a virtualenv running `python3`)
- **Use:** `from parse import get_content
- **Use:** `get_content('some_url_to_usajobs.gov')
- Enjoy

** Disclaimer: You may not hold me liable for misuse of this script.**

