import requests, re, collections
from bs4 import BeautifulSoup

def get_content(url):
	if type(url) != str:
		print('You need to included a string')
		exit()
	else:
		req  = requests.get(url)
		soup = BeautifulSoup(req.content, 'html.parser')

		#zoup = soup.get_text()
		print('\n~**************************************')
		print('\n\n***********QUALIFICATIONS*************\n\n')

		qualifications = soup.find(id="qualifications")
		duties = soup.find(id="duties")
		
		qualification_words = re.findall('\w+', qualifications.get_text())
		dutie_words = re.findall('\w+', duties.get_text())

		qual_word_count = collections.Counter(qualification_words)
		dutie_word_count = collections.Counter(dutie_words)

		#resultgt1 = [word for word in qual_word_count if qual_word_count[word] == 1]
		qualif_gt2 = [word for word in qual_word_count if qual_word_count[word] == 2]
		qualif_gt3 = [word for word in qual_word_count if qual_word_count[word] == 3]
		qualif_gt4 = [word for word in qual_word_count if qual_word_count[word] == 4]
		qualif_gt5 = [word for word in qual_word_count if qual_word_count[word] == 5]

		dutie_gt2 = [word for word in dutie_word_count if dutie_word_count[word] == 2]
		dutie_gt3 = [word for word in dutie_word_count if dutie_word_count[word] == 3]
		dutie_gt4 = [word for word in dutie_word_count if dutie_word_count[word] == 4]
		dutie_gt5 = [word for word in dutie_word_count if dutie_word_count[word] == 5]

		#file saving setup
		qualif_text_file = open("/Users/c0de/Desktop/ITSPECIALIST(INFORMATION SECURITY)___qualifications.txt", "w")
		duties_text_file = open("/Users/c0de/Desktop/ITSPECIALIST(INFORMATION SECURITY)___duties.txt", "w")

		qualif_text_file.write("\n\n~*~*~*~*~*~QUALIFICATIONS~*~*~*~*~*~\n\n")
		
		#qualif == 2
		qualif_text_file.write("\n   ~*~*QUALIFICATIONS == 2~*~*\n")
		for q2 in qualif_gt2:
			qualif_text_file.write("{}\n".format(q2))
		#qualif == 3
		qualif_text_file.write("\n   ~*~*QUALIFICATIONS == 3~*~*\n")
		for q3 in qualif_gt3:
			qualif_text_file.write("{}\n".format(q3))
		#qualif == 4
		qualif_text_file.write("\n   ~*~*QUALIFICATIONS == 4~*~*\n")
		for q4 in qualif_gt4:
			qualif_text_file.write("{}\n".format(q4))

		duties_text_file.write("\n\n~*~*~*~*~*~DUTIES~*~*~*~*~*~\n\n")
		#duties == 2
		duties_text_file.write("\n   ~*~*DUTIES == 2~*~*\n")
		for d2 in dutie_gt2:
			duties_text_file.write("{}\n".format(d2))
		#duties == 3
		duties_text_file.write("\n   ~*~*DUTIES == 3~*~*\n")
		for d3 in dutie_gt3:
			duties_text_file.write("{}\n".format(d3))
		#duties == 4
		duties_text_file.write("\n   ~*~*DUTIES == 4~*~*\n")
		for d4 in dutie_gt4:
			duties_text_file.write("{}\n".format(d4))
		
		print('written.\n')
		#close files.
		qualif_text_file.close()
		duties_text_file.close()
		print('closed.\n')